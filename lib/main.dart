import 'package:cumikemang/list.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Cumi Kemang',
    home: MainMenu(),
  ));
}

class MainMenu extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrangeAccent,
        leading: Icon(Icons.fastfood),
        title: Text('Cumi Kemang'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_forward_ios),
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Profile()));
            },
          )
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: ListView(
            children: <Widget>[
              Card(
                child: ListTile(
                  leading: Icon(Icons.account_circle),
                  title: Text('Profile'),
                  subtitle: Text('Menu Untuk Ganti Password'),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap: (){
                    Navigator.push(context, 
                    MaterialPageRoute(builder: (context)=> MenuProfile()));
                  },
                ),
              ),
              Card(
                child: ListTile(
                  leading: Icon(Icons.book),
                  title: Text('Main Course'),
                  subtitle: Text('Menu Andalan'),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}


class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.deepOrangeAccent,),
      body: ListView(
        children: <Widget>[
          Image.network(
              'https://www.clipartmax.com/png/middle/99-992946_vbs-%C2%AB-fletcher-first-baptist-church-clipart-squid.png')
        ],
      ),
    );
  }
}

