import 'package:flutter/material.dart';

class MenuProfile extends StatefulWidget {
  @override
  _MenuProfileState createState() => _MenuProfileState();
}

class _MenuProfileState extends State<MenuProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrangeAccent,
        title: Text('Profile'),
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(64.0),
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/ckprofile.png'),
                maxRadius: 150.0,
              ),
            ),
            Text(
              'Mrs. Cumi Kemang',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 32.0,fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
